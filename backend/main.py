import requests
import json

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional

from pydantic import BaseModel

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins = ["*"],
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"],
)

@app.get("/")
async def main():
    return{"message": "Hello World"}

class User(BaseModel):
    id: int
    name: str
    email: str
    gender: str
    status: str


class UsersResponse(BaseModel):
    data: list[User] = []
    total: int
    pages: int
    page: int


# API endpoint receiving query parameters and passing them down.
@app.get("/api/users")
def get_users(page: Optional[int] = 1, response_model = UsersResponse):
    response = gorest_handler(page)
    total = response.get("meta").get("pagination").get("total")
    pages = response.get("meta").get("pagination").get("pages")
    page = response.get("meta").get("pagination").get("page")

    data = response.get("data")
    user_list = []
    for user in data:
        user = User(**user)
        user_list.append(user)

    users = UsersResponse(total = total, pages = pages, page = page, data = user_list)
    return users


def gorest_handler(page): 
    url = f'https://gorest.co.in/public/v1/users?page={page}'
    response = requests.get(url)
    return json.loads(response.text)