import React from 'react';
import {Button} from 'reactstrap';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [], page: 1, totalPages: 1};
  }

  componentDidMount() {
      this.fetchData(1)
    }

  fetchData(page) {
    fetch(`http://localhost:8000/api/users?page=${page}`)
    .then(response => response.json())
    .then(data => {
      this.setState({
        data: data.data,
        totalPages: data.total
      })
    })
    .catch(er => console.log(er.message))
  }

  render() {
    return (
      <div className='main'>
        <h1>User Table</h1>
        <div className="App">
          <table>
            <tr>
              <th>Id/(Id-5)</th>
              <th>Id</th>
              <th>Name</th>
              <th>Email</th>
              <th>Gender</th>
              <th>Status</th>
            </tr>
            {this.state.data.map((val, key) => {
              return (
                <tr key={key}>
                  <td>{val.id/(val.id-5)}</td>
                  <td>{val.id}</td>
                  <td>{val.name}</td>
                  <td>{val.email}</td>
                  <td>{val.gender}</td>
                  <td>{val.status}</td>
                </tr>
              )
            })}
          </table>
        </div>
        <div className="Buttons">
          <Button color="primary" size="lg" disabled={this.state.page === 1} onClick={() => {
            const newPage = this.state.page -1
            this.setState({page: newPage})
            this.fetchData(newPage)
            }}>Back
          </Button>
          <p>&nbsp; Page - {this.state.page} &nbsp;</p>
          <Button color="secondary" size="lg" disabled={this.state.page === this.state.totalPages} onClick={() => {
            const newPage = this.state.page +1
            this.setState({page: newPage})
            this.fetchData(newPage)
            }}>Next</Button>
        </div>
      </div>
    );
  }
}

export default App;
