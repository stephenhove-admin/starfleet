## ItGISworx Recruitment Project Solution##

### Contents of the repository ###
------------------------------------

This repository contains the solution to the Recruitment Project.

### Description of solution ###
------------------------------------
This project was implemented using Python and ReactJS.

Python was used for the backend of this project. The fastapi framework
was used to develop the backend. The source code is located in main.py

ReactJS was used for developing the backend of the project.

## Setting up the project ##
### Pre-requisits ###

### fastapi ###
fastapi requires Python 3.6 or later versions.

#### Setting it up ####
Install fastapi in your terminal using the following commands:
- pip install fastapi
- pip install "uvicorn[standard]"

#### Running it ####
Use the following command in your terminal to run the server:
- uvicorn main:app --reload

### React ###

#### Setting it up ####
Install node and npm

- Navigate to the root folder and run: "npm install"

#### Running it ####
To start the service, run the following command in your terminal:
- npm start

### NB: ###
### Please ensure the backend is running first before starting the frondend ###
